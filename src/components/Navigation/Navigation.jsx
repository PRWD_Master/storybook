import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Navigation.scss';
import classnames from 'classnames';

export default class Navigation extends Component {
	render() {
		return (
			<div
				className={classnames('Navigation', {
					'Navigation--sticky': this.props.sticky,
				})}
			>
				<div className="Navigation__Inner">{this.props.children}</div>
			</div>
		);
	}
}

Navigation.propTypes = {
	children: PropTypes.node,
	sticky: PropTypes.bool,
};
