import React from 'react';
import Navigation from './Navigation';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Navigation component', () => {
	it('should render a nav bar', () => {
		let component = mount(<Navigation />);
		expect(component.find('.Navigation').exists()).toBeTruthy();
	});

	it('should apply a sticky class when set as a prop', () => {
		let component = mount(<Navigation sticky={true} />);
		expect(
			component.find('.Navigation').hasClass('Navigation--sticky')
		).toBeTruthy();
	});

	it('should not apply a sticky class by default', () => {
		let component = mount(<Navigation />);
		expect(
			component.find('.Navigation').hasClass('Navigation--sticky')
		).toBeFalsy();
	});

	it('should display child components', () => {
		let component = mount(
			<Navigation>
				<div className="ChildComponent" />
			</Navigation>
		);

		expect(component.find('.ChildComponent').exists()).toBeTruthy();
	});
});
