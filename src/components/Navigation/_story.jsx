import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';

import Navigation from './Navigation';
const stories = storiesOf('Navigation', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

stories.add(
	'default',
	() => (
		<div style={{ height: '4000px' }}>
			<Navigation sticky={boolean('Sticky', false)}>
				<h1>Hello</h1>
				<h3>Test</h3>
			</Navigation>
		</div>
	),
	{
		jest: ['Navigation.test.jsx'],
	}
);

stories.add(
	'sticky',
	() => (
		<div style={{ height: '4000px' }}>
			<Navigation sticky={boolean('Sticky', true)}>
				<h1>Hello</h1>
				<h3>Test</h3>
			</Navigation>
		</div>
	),
	{
		jest: ['Navigation.test.jsx'],
	}
);
