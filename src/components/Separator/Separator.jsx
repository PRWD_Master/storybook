import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './Separator.scss';
export * from './Separator-svg';

export default class Separator extends Component {
	render() {
		const { svg, top } = this.props;

		return (
			<div
				className={classnames('Separator', {
					'Separator--top': top,
				})}
			>
				{svg}
			</div>
		);
	}
}

Separator.propTypes = {
	top: PropTypes.bool,
	svg: PropTypes.oneOfType([PropTypes.element, PropTypes.node]),
};
