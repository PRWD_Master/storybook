import React from 'react';
import Separator, { Circle } from './Separator';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Separator is Separator', () => {
	it('should have svg prop', () => {
		let component = shallow(<Separator svg={<svg />} />);
		expect(component.find('.Separator svg').exists()).toBeTruthy();
	});

	it('should have Separator-svg comp', () => {
		let component = mount(
			<Separator svg={<Circle fill="blue" opacity="0.6" />} />
		);
		expect(component.find('svg.Circle').exists()).toBeTruthy();
	});

	it('should have top class is top is true', () => {
		let component = mount(
			<Separator top={true} svg={<Circle fill="blue" opacity="0.6" />} />
		);
		expect(component.find('.Separator--top').exists()).toBeTruthy();
	});
});
