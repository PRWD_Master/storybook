import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';

import Separator, {
	Circle,
	Triangle,
	Curve,
	CurveDouble,
	TriangleShadow,
	WaveAnimated,
	Wave,
	SmallCurve
} from './Separator';
const stories = storiesOf('Separator', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

stories.add('Circle', () => <Separator svg={<Circle fill="blue" />} />, {
	jest: ['Separator.test.jsx'],
});

stories.add('Triangle', () => <Separator svg={<Triangle fill="yellow" />} />, {
	jest: ['Separator.test.jsx'],
});

stories.add('Curve', () => <Separator svg={<Curve fill="red" />} />, {
	jest: ['Separator.test.jsx'],
});

stories.add('SmallCurve', () => <Separator top={true} svg={<SmallCurve fill="red" />} />, {
	jest: ['Separator.test.jsx'],
});

stories.add(
	'CurveDouble',
	() => (
		<Separator
			svg={
				<CurveDouble
					fill="red"
					opacity="0.7"
					secondaryFill="blue"
					secondaryOpacity="0.7"
				/>
			}
		/>
	),
	{
		jest: ['Separator.test.jsx'],
	}
);

stories.add(
	'WaveAnimated',
	() => <Separator top={true} svg={<WaveAnimated fill="purple" />} />,
	{
		jest: ['Separator.test.jsx'],
	}
);

stories.add(
	'TriangleShadow',
	() => (
		<Separator
			svg={<TriangleShadow fill="green" secondaryFill="lightgreen" />}
		/>
	),
	{
		jest: ['Separator.test.jsx'],
	}
);

stories.add('Wave', () => <Separator svg={<Wave fill="blue" />} />, {
	jest: ['Separator.test.jsx'],
});

stories.add(
	'Top',
	() => <Separator top={true} svg={<Circle fill="blue" />} />,
	{
		jest: ['Separator.test.jsx'],
	}
);

stories.add(
	'Opacity',
	() => <Separator svg={<Circle fill="blue" opacity="0.6" />} />,
	{
		jest: ['Separator.test.jsx'],
	}
);

const divStyle = {
	backgroundColor: 'blue',
	padding: '2em',
};

const h1Style = {
	color: 'white',
	fontSize: '2em',
	textAlign: 'center',
};

const pStyle = {
	color: 'white',
	fontSize: '1em',
};
stories.add(
	'WithContent',
	() => (
		<section>
			<Separator top={true} svg={<Circle fill="blue" />} />
			<div style={divStyle}>
				<h1 style={h1Style}>testing</h1>
				<p style={pStyle}>
					Lorem Ipsum is simply dummy text of the printing and typesetting
					industry. Lorem Ipsum has been the industry's standard dummy text ever
					since the 1500s, when an unknown printer took a galley of type and
					scrambled it to make a type specimen book. It has survived not only
					five centuries, but also the leap into electronic typesetting,
					remaining essentially unchanged. It was popularised in the 1960s with
					the release of Letraset sheets containing Lorem Ipsum passages, and
					more recently with desktop publishing software like Aldus PageMaker
					including versions of Lorem Ipsum.
				</p>
			</div>
			<Separator svg={<Circle fill="blue" />} />
		</section>
	),
	{
		jest: ['Separator.test.jsx'],
	}
);
stories.add(
	'WithContentAnimated',
	() => (
		<section>
			<Separator svg={<WaveAnimated fill="blue" />} />
			<div style={divStyle}>
				<h1 style={h1Style}>testing</h1>
				<p style={pStyle}>
					Lorem Ipsum is simply dummy text of the printing and typesetting
					industry. Lorem Ipsum has been the industry's standard dummy text ever
					since the 1500s, when an unknown printer took a galley of type and
					scrambled it to make a type specimen book. It has survived not only
					five centuries, but also the leap into electronic typesetting,
					remaining essentially unchanged. It was popularised in the 1960s with
					the release of Letraset sheets containing Lorem Ipsum passages, and
					more recently with desktop publishing software like Aldus PageMaker
					including versions of Lorem Ipsum.
				</p>
			</div>
			<Separator top={true} svg={<WaveAnimated fill="blue" />} />
		</section>
	),
	{
		jest: ['Separator.test.jsx'],
	}
);
