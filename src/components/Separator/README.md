# Search Component

## Use

`svg` Requires a component or svg node

file: `Separator-svg.jsx` contains all separator svgs

To use import `svg const` from file.

Example
import {Circle} from '.Separator-svg.jsx'

<Separator svg={
<Circle fill="blue" />
}>

`Separator-svg.jsx` props

all will take: `fill`, `stroke`, `opacity`
Some will take: `secondaryFill`, `secondaryStroke`, `secondaryOpacity`

`top` requires boolean ? svg separator will rotate 180 : nothing

## Example

-- For top use

<section>
    <Separator top={true} />
    <div>
        {content}
    </div>
</section>

-- For bottom use

<section>
    <div>
        {content}
    </div>
    <Separator />
</section>

-- For both use

<section>
    <Separator top={true} />
    <div>
        {content}
    </div>
     <Separator />
</section>

<Separator top={true}
svg={<Circle fill="blue" stroke="blue" opacity="0.7" />}
/>
