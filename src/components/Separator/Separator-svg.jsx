import React from 'react';

const Circle = ({ fill, stroke, opacity }) => {
    return (
        <svg className="Circle" xmlns="http://www.w3.org/2000/svg" fill={fill} stroke={stroke} opacity={opacity} viewBox="0 0 150 27">
            <path d="M150,0C90,18,60,18,0,0Z" />
        </svg>
    );
};

const Triangle = ({ fill, stroke, opacity }) => {
    return (
        <svg className="Triangle" xmlns="http://www.w3.org/2000/svg" fill={fill} stroke={stroke} opacity={opacity} version="1.1" width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none">
            <path d="M0 0 L50 100 L100 0 Z"></path>
        </svg>
    )
}

const Curve = ({ fill, stroke, opacity }) => {
    return (
        <svg className="Curve" xmlns="http://www.w3.org/2000/svg" fill={fill} stroke={stroke} opacity={opacity} version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path d="M0 0 C 50 100 80 100 100 0 Z"></path>
        </svg>
    )
}

const SmallCurve = ({ fill, opacity }) => {
    return (
        <svg className="SmallCurve" xmlns="http://www.w3.org/2000/svg" transform="rotate(180 0 0)" fill={fill} opacity={opacity} width="100%" height="100%" viewBox="0 0 528.395 185.563">
            <path d="M428.395,27.88C377.939,5.262,289.825-13.633,162.416,13.093S-66.925-24.389-100,22.636V185.563H428.395Z" transform="translate(100)"></path>
        </svg>
    )
}

const TriangleShadow = ({ fill, stroke, opacity, secondaryFill, secondaryStroke, SecondaryOpacity }) => {
    return (
        <svg className="TriangleShadow" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
            <path fill={fill} stroke={stroke} opacity={opacity} className="TriangleShadowPath1" d="M0 0 L50 100 L100 0 Z"></path>
            <path fill={secondaryFill} stroke={secondaryStroke} opacity={SecondaryOpacity} className="TriangleShadowPath2" d="M50 100 L100 40 L100 0 Z"></path>
        </svg>
    )
}
const Wave = ({ fill, stroke, opacity }) => {
    return (
        <svg className="Wave" width="100%" height="100%" transform="rotate(180 0 0)" fill={fill} stroke={stroke} opacity={opacity} version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1922.95 164.14">
            <path d="M1920,154.12S1320.89,112,850.87,151.21C430.26,186.26.27,136,.27,136L-3,298.67H1919.82Z" transform="translate(3 -134.52)" />
        </svg>
    )
}

const CurveDouble = ({ fill, opacity, secondaryFill, SecondaryOpacity }) => {
    return (
        <svg className="CurveDouble" xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" transform="rotate(180 0 0)" version="1.1" viewBox="0 0 300.85 23.78">
            <path fill={fill} opacity={opacity} d="M152.81 13.8a300.92 300.92 0 0 1-39.31-3.83C70.85 2.44.02 0 .02 0l.06 7.79c1.72.42 68.2 16.32 152.73 6.01zM300.85 23.76h-16.08l16.08.02v-.02z" />
            <path fill={secondaryFill} opacity={SecondaryOpacity} d="M300.85 23.76L300.29.01S225.75 1.2 179.65 9.7c-9.1 1.68-18.07 3-26.84 4.1C68.28 24.11 1.81 8.21.08 7.8H.03v16h300.82z" />
        </svg>
    )
}
const WaveAnimated = ({ fill, stroke, opacity }) => {
    return (
        <svg className="WaveAnimated" width="100%" height="100%" transform="rotate(0 0 0)" version="1.1" fill={fill} stroke={stroke} opacity={opacity}>
            <path d="M 0 47.6467 C 291.035 85.2057 436.959 99.3543 779.018 34.9941 C 1074.87 -22.9542 1252.29 8.92693 1476.55 30.5001 C 1983.64 70.8519 2189.29 108.451 2560 47.6469 V 193.648 L 0 193.648 V 47.6467 Z">
                <animate repeatCount="indefinite" fill="freeze" attributeName="d" dur="30s" values="M0 25.9086C277 84.5821 433 65.736 720 25.9086C934.818 -3.9019 1214.06 -5.23669 1442 8.06597C2079 45.2421 2208 63.5007 2560 25.9088V171.91L0 171.91V25.9086Z; M0 86.3149C316 86.315 444 159.155 884 51.1554C1324 -56.8446 1320.29 34.1214 1538 70.4063C1814 116.407 2156 188.408 2560 86.315V232.317L0 232.316V86.3149Z; M0 53.6584C158 11.0001 213 0 363 0C513 0 855.555 115.001 1154 115.001C1440 115.001 1626 -38.0004 2560 53.6585V199.66L0 199.66V53.6584Z; M0 25.9086C277 84.5821 433 65.736 720 25.9086C934.818 -3.9019 1214.06 -5.23669 1442 8.06597C2079 45.2421 2208 63.5007 2560 25.9088V171.91L0 171.91V25.9086Z"></animate>
            </path>
        </svg>
    )
}


export {
    Circle,
    Curve,
    CurveDouble,
    SmallCurve,
    Triangle,
    TriangleShadow,
    Wave,
    WaveAnimated
};
