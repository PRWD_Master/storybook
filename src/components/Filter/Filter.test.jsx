import { createFilter } from "./Filter.js";

describe("Filter", () => {
  describe("filter.js", () => {
    it("should return a function based on the object", () => {
      let dummyData = [
        { name: "Block of Edam", price: 6 },
        { name: "Block of Wensleydale", price: 9 },
        { name: "Block of Cheddar", price: 5 },
        { name: "Red Leicester", price: 3.2 },
        { name: "Stilton", price: 1.23 },
        { name: "Wheel", price: 50 }
      ];
      let filter = createFilter([{ property: "name", value: "Block" }]);
    });
  });
});
