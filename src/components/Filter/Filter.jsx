import React, { Component } from "react";
import PropTypes from "prop-types";
import { createSorter } from "./Sort";
import { createFilter } from "./Filter.js";

export default class Filter extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    this.setState({ data: this.props.data });
  }

  render() {
    const { data } = this.state;

    return data ? this.renderData(data) : this.renderLoading();
  }

  renderData(data) {
    const { component } = this.props;

    if (data.length) {
      const { filters, sorters } = this.props;

      if (Array.isArray(filters) && filters.length) {
        data = data.filter(item => {
          const func = createFilter(...filters);
          return func(item);
        });
      }

      if (Array.isArray(sorters) && sorters.length) {
        data.sort(createSorter(...sorters));
      }

      return (
        <div>
          {data.map(item => {
            return component(item);
          })}
        </div>
      );
    } else {
      return <div>No items found</div>;
    }
  }

  renderLoading() {
    return <div>Loading...</div>;
  }
}

Filter.propTypes = {
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  children: PropTypes.node
};
