import React from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs } from "@storybook/addon-knobs";
import { withTests } from "@storybook/addon-jest";
import results from "../../../jest-test-results.json";

import Filter from "./Filter.jsx";
import Button from "../Button/Button.jsx";

const stories = storiesOf("Filter", module)
  .addDecorator(withKnobs)
  .addDecorator(withTests({ results }));

let dummyData = [
  { name: "Block of Edam", price: 6 },
  { name: "Block of Wensleydale", price: 9 },
  { name: "Block of Cheddar", price: 5 },
  { name: "Red Leicester", price: 3.2 },
  { name: "Stilton", price: 1.23 },
  { name: "Wheel", price: 50 }
];
const filters = [{ property: "name", value: "Block" }];
const sorters = [{ property: "price", direction: "DESC" }];

stories.add(
  "filter",
  () => {
    const listItem = item => {
      return (
        <div key={`${item.name}-${item.price}`}>
          <a href="">{item.name}</a>
          {item.price}
        </div>
      );
    };

    return (
      <div>
        <h3>Filtered + sorted</h3>
        <Filter
          data={dummyData}
          filters={filters}
          sorters={sorters}
          component={listItem}
        />
        <hr />
        <h3>Filtered + unsorted</h3>
        <Filter data={dummyData} filters={filters} component={listItem} />
      </div>
    );
  },
  {
    jest: ["Filter.test.jsx"]
  }
);

stories.add(
  "sorted",
  () => {
    const listItem = item => {
      return (
        <div key={`${item.name}-${item.price}`}>
          <a href="">{item.name}</a>
          {item.price}
        </div>
      );
    };

    return (
      <div>
        <h3>sorted only</h3>
        <Filter data={dummyData} sorters={sorters} component={listItem} />
      </div>
    );
  },
  {
    jest: ["Filter.test.jsx"]
  }
);

stories.add(
  "button",
  () => {
    const listItem = item => {
      return (
        <Button>
          {item.name} (£{item.price})
        </Button>
      );
    };

    return (
      <div>
        <h3>sorted only</h3>
        <Filter data={dummyData} sorters={sorters} component={listItem} />
      </div>
    );
  },
  {
    jest: ["Filter.test.jsx"]
  }
);
