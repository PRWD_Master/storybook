import React from 'react';
import TestimonialCard from './TestimonialCard';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp } from '@fortawesome/pro-solid-svg-icons';

describe('TestimonialCard', () => {
	it('should display a card without quote, or link by defualt', () => {
		let component = mount(<TestimonialCard />);
		expect(component.find('.TestimonialCard').exists()).toBeTruthy();
		expect(component.find('.TestimonialCard__Quote').exists()).toBeFalsy();
		expect(component.find('.TestimonialCard__ReadMore').exists()).toBeFalsy();
	});

	it('should display the given title', () => {
		let component = mount(<TestimonialCard title="Title" />);
		expect(component.find('.TestimonialCard__Title').text()).toBe('Title');
	});

	it('should display the given subtitle', () => {
		let component = mount(<TestimonialCard subtitle="Subtitle" />);
		expect(component.find('.TestimonialCard__Subtitle').text()).toBe(
			'Subtitle'
		);
	});

	it('should display the given body', () => {
		let component = mount(
			<TestimonialCard content={{ __html: '<b>test</b>' }} />
		);
		expect(component.find('.TestimonialCard__Body').text()).toBe('test');
	});

	it('should display the a quote icon when passed as a prop', () => {
		let component = mount(
			<TestimonialCard
				subtitle="Subtitle"
				quoteIcon={<FontAwesomeIcon icon={faThumbsUp} />}
			/>
		);
		expect(component.find('.TestimonialCard__Quote').exists()).toBeTruthy();
	});

	it('should display the read more link and text', () => {
		let component = mount(
			<TestimonialCard readMoreLink="google.com" readMoreText="Visit Google" />
		);
		expect(component.find('.TestimonialCard__ReadMore').exists()).toBeTruthy();
		expect(component.find('.TestimonialCard__ReadMore').text()).toBe(
			'Visit Google'
		);
		expect(component.find('a').props().href).toBe('google.com');
	});
});
