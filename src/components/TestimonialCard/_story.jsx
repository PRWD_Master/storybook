import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faQuoteLeft } from '@fortawesome/pro-solid-svg-icons';

import TestimonialCard from './TestimonialCard';
const stories = storiesOf('TestimonialCard', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

stories.add(
	'default',
	() => (
		<TestimonialCard
			title="Paul Roberts"
			subtitle="Bristol"
			image="http://www.w3schools.com/css/trolltunga.jpg"
			content={{
				__html:
					'dfkhjdfs sdf sdg sg rt fg sd dsfsdf sdfsdf sdfsdf sdf sdf sdfdsf ',
			}}
			quoteIcon={<FontAwesomeIcon icon={faQuoteLeft} />}
			readMoreText="Read their story"
			readMoreLink="http://google.com"
		/>
	),
	{
		jest: ['TestimonialCard.test.jsx'],
	}
);

stories.add(
	'no link',
	() => (
		<TestimonialCard
			title="Paul Roberts"
			subtitle="Bristol"
			image="http://www.w3schools.com/css/trolltunga.jpg"
			content={{
				__html:
					'dfkhjdfs sdf sdg sg rt fg sd dsfsdf sdfsdf sdfsdf sdf sdf sdfdsf ',
			}}
			quoteIcon={<FontAwesomeIcon icon={faQuoteLeft} />}
			readMoreText="Read their story"
		/>
	),
	{
		jest: ['TestimonialCard.test.jsx'],
	}
);

stories.add(
	'no quote',
	() => (
		<TestimonialCard
			title="Paul Roberts"
			subtitle="Bristol"
			image="http://www.w3schools.com/css/trolltunga.jpg"
			content={{
				__html:
					'dfkhjdfs sdf sdg sg rt fg sd dsfsdf sdfsdf sdfsdf sdf sdf sdfdsf ',
			}}
			readMoreText="Read their story"
			readMoreLink="http://google.com"
		/>
	),
	{
		jest: ['TestimonialCard.test.jsx'],
	}
);
