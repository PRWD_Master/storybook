import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './TestimonialCard.scss';

export default class TestimonialCard extends Component {
	render() {
		const {
			title,
			subtitle,
			content,
			readMoreLink,
			readMoreText,
			quoteIcon,
		} = this.props;

		const style = {
			backgroundImage: 'url(' + this.props.image + ')',
			backgroundSize: 'cover',
			backgroundPosition: this.props.imagePosition,
		};

		return (
			<div className="TestimonialCard">
				<div className="TestimonialCard__Header">
					<div className="TestimonialCard__Image" style={style} />
				</div>
				<div className="TestimonialCard__Content">
					{quoteIcon && (
						<div className="TestimonialCard__Quote">{quoteIcon}</div>
					)}
					<div className="TestimonialCard__Title">
						<h3>{title}</h3>
					</div>
					<div className="TestimonialCard__Subtitle">
						<h4>{subtitle}</h4>
					</div>
					<div
						className="TestimonialCard__Body"
						dangerouslySetInnerHTML={content}
					/>
					{readMoreLink && (
						<div className="TestimonialCard__ReadMore">
							<a href={readMoreLink}>
								{readMoreText ? readMoreText : 'Read More'}
							</a>
						</div>
					)}
				</div>
			</div>
		);
	}
}

TestimonialCard.propTypes = {
	title: PropTypes.string,
	subtitle: PropTypes.string,
	content: PropTypes.object,
	image: PropTypes.object,
	quoteIcon: PropTypes.object,
	readMoreText: PropTypes.string,
	readMoreLink: PropTypes.string,
};
