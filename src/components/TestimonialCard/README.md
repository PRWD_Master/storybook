# TestimonialCard Component

- Testimonial Card that has an image and title.

```
    <TestimonialCard
        title="Mark Markington"
        subtitle="Bristol"
        content={ __html: "<b>DangerouslySetInnerHTML</b>" },
        image={ ... },
        quoteIcon="",
        readMoreText="Read More",
        readMoreLink="/testimonials/1",
    >

```
