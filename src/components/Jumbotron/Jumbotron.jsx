import React, { Component } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import './Jumbotron.scss';

export default class Jumbotron extends Component {
	render() {
		const bgStyle = {
			backgroundImage: 'url(' + this.props.image + ')',
			backgroundSize: 'cover',
			backgroundPosition: this.props.imagePosition,
			width: '100%',
			height: '100%',
			position: 'absolute',
		};

		const contStyle = {
			position: 'relative',
		}

		return (
			<div
				className={classnames('Jumbotron', this.props.className)}
				style={contStyle}
			>
				<div className="Jumbotron__Background" style={bgStyle}></div>
				<div
					className={`Jumbotron__Inner Jumbotron__Inner--${
						this.props.textPosition
						}`}
					style={contStyle}
				>
					{this.props.children}
				</div>
			</div>
		);
	}
}

Jumbotron.propTypes = {
	children: PropTypes.node,
	className: PropTypes.string,
	image: PropTypes.string,
	imagePosition: PropTypes.string,
	textPosition: PropTypes.string,
};
