import React from 'react';
import Jumbotron from './Jumbotron';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Jumbotron is Jumbotron', () => {
	it('should display a jumbotron', () => {
		let component = shallow(<Jumbotron />);
		expect(component.find('.Jumbotron').exists()).toBeTruthy();
		expect(component.find('.Jumbotron__Inner').exists()).toBeTruthy();
	});

	it('should set the background image', () => {
		let img = 'http://test.com/test.jpg';
		let component = mount(<Jumbotron image={img} />);

		expect(component.find('.Jumbotron__Background').props().style.backgroundImage).toBe(
			'url(http://test.com/test.jpg)'
		);
	});

	it('should set the float of the text', () => {
		let component = mount(<Jumbotron textPosition="right" />);
		expect(component.find('.Jumbotron__Inner--right').exists()).toBeTruthy();
		expect(component.find('.Jumbotron__Inner--left').exists()).toBeFalsy();
	});
});
