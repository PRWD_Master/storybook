import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';
import Button from '../Button';
import Jumbotron from './Jumbotron';
const stories = storiesOf('Jumbotron', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

stories.add(
	'default',
	() => (
		<Jumbotron
			className="Jumbotron--dark"
			image="http://www.w3schools.com/css/trolltunga.jpg"
		>
			<h1>This is a Jumbotron</h1>
			<h2>Find out more from this very long subtitle</h2>
			<Button>Find out more</Button>
		</Jumbotron>
	),
	{
		jest: ['Jumbotron.test.jsx'],
	}
);

stories.add(
	'PRWD',
	() => (
		<Jumbotron
			className="Jumbotron--PRWD"
			image="https://www.prwd.co.uk/wp-content/uploads/work_enviroment_prwd_1-1-640x400.jpg"
			imagePosition="center"
			textPosition="center"
		>
			<h1>This is a Jumbotron</h1>
			<h2>Find out more from this very long subtitle</h2>
			<Button primary="true">Find out more</Button>
		</Jumbotron>
	),
	{
		jest: ['Jumbotron.test.jsx'],
	}
);
