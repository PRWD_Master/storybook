# Jumbotron Component

- full width jumbotron that takes a background image, and children left, center or right aligned

### Props

- children: PropTypes.node,
- className: PropTypes.string,
- image: PropTypes.string,
- imagePosition: PropTypes.string,
- textPosition: PropTypes.string,
