import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number, date } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';

import results from '../../../jest-test-results.json';
import ExperimentCard from './ExperimentCard';

const stories = storiesOf('ExperimentCard', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

const cardObj = () => {
	return {
		client: text('client', 'PRWD'),
		test_id: text('Test ID', 'P1001'),
		test_name: text('Test Name', 'homepage optimization'),
		test_status: text('Test Status', 'win'),
		date_start: text('Start Date', 'Jan 2019'),
		primary_metric: text('Primary Metric', 'CR'),
		primary_metric_uplift: text('Primary Metric Uplift', '105%'),
		secondary_metric: text('Secondary Metric', 'add to basket'),
	};
};

stories.add('default', () => <ExperimentCard {...cardObj()} />, {
	jest: ['ExperimentCard.test.jsx'],
});

const winObj = () => {
	return {
		client: text('client', 'harveys'),
		test_id: text('Test ID', 'ST1001'),
		test_name: text('Test Name', 'homepage optimization'),
		test_status: text('Test Status', 'win'),
		date_start: text('Start Date', 'Jan 2019'),
		primary_metric: text('Primary Metric', 'CR'),
		primary_metric_uplift: text('Primary Metric Uplift', '284%'),
		secondary_metric: text('Secondary Metric', 'add to basket'),
	};
};

stories.add('win', () => <ExperimentCard {...winObj()} />, {
	jest: ['ExperimentCard.test.jsx'],
});

const lossObj = () => {
	return {
		client: text('client', 'suntransfers'),
		test_id: text('Test ID', 'ST1001'),
		test_name: text('Test Name', 'homepage optimization'),
		test_status: text('Test Status', 'loss'),
		date_start: text('Start Date', 'Jan 2019'),
		primary_metric: text('Primary Metric', 'CR'),
		primary_metric_uplift: text('Primary Metric Uplift', '-1%'),
		secondary_metric: text('Secondary Metric', 'add to basket'),
	};
};

stories.add('loss', () => <ExperimentCard {...lossObj()} />, {
	jest: ['ExperimentCard.test.jsx'],
});

const runningObj = () => {
	return {
		client: text('client', 'mossbros'),
		test_id: text('Test ID', 'M1001'),
		test_name: text('Test Name', 'homepage optimization'),
		test_status: text('Test Status', 'running'),
		date_start: text('Start Date', 'Jan 2019'),
		primary_metric: text('Primary Metric', 'CR'),
		primary_metric_uplift: text('Primary Metric Uplift', '1%'),
		secondary_metric: text('Secondary Metric', 'add to basket'),
	};
};

stories.add('running', () => <ExperimentCard {...runningObj()} />, {
	jest: ['ExperimentCard.test.jsx'],
});

const axedObj = () => {
	return {
		client: text('client', 'vaping'),
		test_id: text('Test ID', 'V1001'),
		test_name: text('Test Name', 'homepage optimization'),
		test_status: text('Test Status', 'axed'),
		date_start: text('Start Date', 'Jan 2019'),
		primary_metric: text('Primary Metric', 'AOV'),
		primary_metric_uplift: text('Primary Metric Uplift', '1%'),
		secondary_metric: text('Secondary Metric', 'Interaction'),
	};
};

stories.add('axed', () => <ExperimentCard {...axedObj()} />, {
	jest: ['ExperimentCard.test.jsx'],
});

const iterateObj = () => {
	return {
		client: text('client', 'bensons'),
		test_id: text('Test ID', 'B1001'),
		test_name: text('Test Name', 'homepage optimization'),
		test_status: text('Test Status', 'iterate'),
		date_start: text('Start Date', 'Jan 2019'),
		primary_metric: text('Primary Metric', 'AOV'),
		primary_metric_uplift: text('Primary Metric Uplift', '0.7%'),
		secondary_metric: text('Secondary Metric', 'Interaction'),
	};
};

stories.add('iterate', () => <ExperimentCard {...iterateObj()} />, {
	jest: ['ExperimentCard.test.jsx'],
});
