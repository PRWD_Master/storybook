import React, { Component } from 'react';
import './ExperimentCard.scss';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
	faThumbsUp,
	faThumbsDown,
	faHourglassHalf,
	faQuestionCircle,
	faTimesCircle,
	faCalendar,
} from '@fortawesome/pro-solid-svg-icons';

export default class ExperimentCard extends Component {
	constructor() {
		super();
		this.state = {
			data: [],
		};
	}

	parseDate(dateString) {
		const monthNames = [
			'Jan',
			'Feb',
			'Mar',
			'Apr',
			'May',
			'Jun',
			'Jul',
			'Aug',
			'Sept',
			'Oct',
			'Nov',
			'Dec',
		];

		const date = new Date(dateString);
		const monthIndex = date.getMonth();
		const year = date.getFullYear();

		return date == 'Invalid Date'
			? 'Invalid'
			: monthNames[monthIndex] + ' ' + year;
	}

	testStatus(test_status, primary_metric_uplift) {
		switch (test_status) {
			case 'running':
				return 'Running';
			case 'iterate':
				return 'Iterate';
			case 'axed':
				return 'Axed';
			case 'loss':
				return primary_metric_uplift;
			case 'win':
				let plus = `+${primary_metric_uplift}`;
				return plus;
			default:
				return 'Unknown';
		}
	}

	testStatusIcon(test_status) {
		switch (test_status) {
			case 'running':
				return faHourglassHalf;
			case 'iterate':
				return faQuestionCircle;
			case 'axed':
				return faTimesCircle;
			case 'loss':
				return faThumbsDown;
			case 'win':
				return faThumbsUp;
			default:
				return faQuestionCircle;
		}
	}

	render() {
		let statusIcon, statusColor;
		const {
			client,
			test_id,
			test_name,
			test_status,
			primary_metric_uplift,
			date_start,
			primary_metric,
			secondary_metric,
			test_analysis,
		} = this.props;

		return (
			<div className="ExperimentCard">
				<div className={`ExperimentCard__top ExperimentCard__top--${client}`}>
					<span
						className={`ExperimentCard__overlay ExperimentCard__overlay--${client}`}
					/>
					<img
						src={`/images/ExperimentCard/${client}.svg`}
						className={`ExperimentCard__logo`}
					/>
				</div>
				<div className="ExperimentCard__middle">
					<h4 className="ExperimentCard__id">{test_id}</h4>
					<h4 className="ExperimentCard__name">{test_name}</h4>
					<ul className="ExperimentCard__info">
						<li
							className={`ExperimentCard__uplift ExperimentCard__uplift--${test_status}`}
						>
							<span className="ExperimentCard__uplift-icon">
								<FontAwesomeIcon icon={this.testStatusIcon(test_status)} />
							</span>
							<span className="ExperimentCard__uplift-text">
								{this.testStatus(test_status, primary_metric_uplift)}
							</span>
						</li>
						<li className="ExperimentCard__start-date">
							<span className="ExperimentCard__start-date-icon">
								<FontAwesomeIcon icon={faCalendar} color="#707070" />
							</span>
							<span className="ExperimentCard__start-date-text">
								{this.parseDate(date_start)}
							</span>
						</li>
						<li
							className={`ExperimentCard__pm ExperimentCard__pm--${test_status}`}
						>
							{primary_metric}
						</li>
						<li className="ExperimentCard__sm">{secondary_metric}</li>
					</ul>
				</div>
				<div
					className={`ExperimentCard__bottom ExperimentCard__bottom--${client}`}
				>
					<a src={test_analysis} className="ExperimentCard__view">
						view test results
					</a>
				</div>
			</div>
		);
	}
}

ExperimentCard.propTypes = {
	client: PropTypes.string,
	test_id: PropTypes.string,
	test_name: PropTypes.string,
	test_status: PropTypes.string,
	primary_metric_uplift: PropTypes.string,
	date_start: PropTypes.string,
	primary_metric: PropTypes.string,
	secondary_metric: PropTypes.string,
	test_analysis: PropTypes.string,
};
