import React from 'react';
import ExperimentCard from './ExperimentCard';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('ExperimentCard', () => {
    it('should set the background image to reflect client prop', () => {
        let component = shallow(<ExperimentCard client='prwd'/>)
        expect(component.find('.ExperimentCard__top--prwd').exists()).toBeTruthy();
    })

    it('should set the client image to reflect client prop', () => {
        let component = shallow(<ExperimentCard client='prwd'/>)
        expect(component.find('.ExperimentCard__top img').prop("src")).toEqual('/images/ExperimentCard/prwd.svg');
    })

    it('should set the colour strip to reflect the client', () => {
        let component = shallow(<ExperimentCard client='prwd'/>)
        expect(component.find('.ExperimentCard__bottom').hasClass('ExperimentCard__bottom--prwd')).toBeTruthy();
    })

    describe('Test Status', () => {

        it('should say if the test is running, axed, iterate', () => {
            let component = shallow(<ExperimentCard test_status='running'/>)
            expect(component.find('.ExperimentCard__uplift-text').text()).toEqual('Running');

            component = shallow(<ExperimentCard test_status='axed'/>)
            expect(component.find('.ExperimentCard__uplift-text').text()).toEqual('Axed');

            component = shallow(<ExperimentCard test_status='iterate'/>)
            expect(component.find('.ExperimentCard__uplift-text').text()).toEqual('Iterate');
        })

        it('should show icon for running, axed, iterate', () => {
            let component = mount(<ExperimentCard test_status='running'/>)
            expect(component.find('.ExperimentCard__uplift-icon [data-icon="hourglass-half"]').exists()).toBeTruthy();

            component = mount(<ExperimentCard test_status='axed'/>)
            expect(component.find('.ExperimentCard__uplift-icon [data-icon="times-circle"]').exists()).toBeTruthy();

            component = mount(<ExperimentCard test_status='iterate'/>)
            expect(component.find('.ExperimentCard__uplift-icon [data-icon="question-circle"]').exists()).toBeTruthy();
        })

        it('should display uplift for win and loss', () => {
            let component = shallow(<ExperimentCard test_status='win' primary_metric_uplift="56"/>)
            expect(component.find('.ExperimentCard__uplift-text').text()).toEqual('+56');

            component = shallow(<ExperimentCard test_status='loss' primary_metric_uplift="-56"/>)
            expect(component.find('.ExperimentCard__uplift-text').text()).toEqual('-56');
        })

        it('should show icon for win, loss', () => {
            let component = shallow(<ExperimentCard test_status='win'/>)
            expect(component.find('.ExperimentCard__start-date-icon [data-icon="thumbs-up"]')).toBeTruthy();

            component = shallow(<ExperimentCard test_status='loss'/>)
            expect(component.find('.ExperimentCard__start-date-icon [data-icon="thumbs-down"]')).toBeTruthy();
        })
        
    });

    describe('Date parser', () => {
        it('should parse full dates', () => {
            let component = shallow(<ExperimentCard date_start="2018-04-05"/>)
            expect(component.find('.ExperimentCard__start-date-text').text()).toEqual('Apr 2018');
        });

        it('should parse short dates', () => {
            let component = shallow(<ExperimentCard date_start="January 2018"/>)
            expect(component.find('.ExperimentCard__start-date-text').text()).toEqual('Jan 2018');
        });

        it('should parse slashed dates', () => {
            let component = shallow(<ExperimentCard date_start="12/12/2012"/>)
            expect(component.find('.ExperimentCard__start-date-text').text()).toEqual('Dec 2012');
        });

        it('should have calendar icon', () => {
            let component = mount(<ExperimentCard />)
            expect(component.find('.ExperimentCard__start-date-icon [data-icon="calendar"]').exists()).toBeTruthy();
        });

    });
})