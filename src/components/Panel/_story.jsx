import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';

import Panel from './Panel';
const stories = storiesOf('Panel', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

const fullObj = () => {
	return {
		children: <h1> test </h1>,
		size: 'full',
	};
};

stories.add('full', () => <Panel {...fullObj()} />, {
	jest: ['Panel.test.jsx'],
});

const halfObj = () => {
	return {
		children: <h1> test </h1>,
		size: 'half',
	};
};

stories.add('half', () => <Panel {...halfObj()} />, {
	jest: ['Panel.test.jsx'],
});

const thirdObj = () => {
	return {
		children: <h1> test </h1>,
		size: 'third',
	};
};

stories.add('third', () => <Panel {...thirdObj()} />, {
	jest: ['Panel.test.jsx'],
});

const quaterObj = () => {
	return {
		children: <h1> test </h1>,
		size: 'quater',
	};
};

stories.add('quater', () => <Panel {...quaterObj()} />, {
	jest: ['Panel.test.jsx'],
});
