import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Panel.scss';

export default class Panel extends Component {
	render() {
		const { children, size } = this.props;

		return (
			<div className={`Panel Panel-${size}`}>
				<div className="Panel__content">{children}</div>
			</div>
		);
	}
}

Panel.propTypes = {
	children: PropTypes.node,
	size: PropTypes.string,
};
