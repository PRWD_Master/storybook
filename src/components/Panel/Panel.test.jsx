import React from 'react';
import Panel from './Panel';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Panel is Panel', () => {
	it('should consist of some options', () => {
		let component = mount(<Panel />);
		expect(component.find('Panel').exists()).toBeTruthy();
	});
});

describe('Panel', () => {
	it('panel size should reflect panel width', () => {
		let component = shallow(<Panel size="full" />);
		expect(component.find('.Panel-full').exists()).toBeTruthy();
	});
});
