import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/pro-solid-svg-icons';

import RadioPanel from './RadioPanel';

const stories = storiesOf('RadioPanel', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

const func = (radio, event) => {
	console.info('RadioPanel', 'Callback was called: ', radio, event);
};
const defaultData = [
	{
		label: 1,
		value: 1,
	},
	{
		label: 2,
		value: 2,
	},
	{
		label: 3,
		value: 3,
	},
];

const iconData = [
	{
		label: 'first',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '1',
	},
	{
		label: 'second',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '2',
	},
	{
		label: 'third',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '3',
	},
	{
		label: 'fourth',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '4',
	},
	{
		label: 'fifth',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '5',
	},
];

const childrenData = [
	{
		children: (
			<div>
				<FontAwesomeIcon icon={faSearch} />
				<h3>radio 1</h3>
				<h5>blah blah blah</h5>
			</div>
		),
		value: '1',
	},
	{
		children: (
			<div>
				<FontAwesomeIcon icon={faSearch} />
				<h3>radio 2</h3>
				<h5>blah blah blah</h5>
			</div>
		),
		value: '2',
	},
	{
		children: (
			<div>
				<FontAwesomeIcon icon={faSearch} />
				<h3>radio 3</h3>
				<h5>blah blah blah</h5>
			</div>
		),
		value: '3',
	},
	{
		children: (
			<div>
				<FontAwesomeIcon icon={faSearch} />
				<h3>radio 4</h3>
				<h5>blah blah blah</h5>
			</div>
		),
		value: '4',
	},
	{
		children: (
			<div>
				<FontAwesomeIcon icon={faSearch} />
				<h3>radio 5</h3>
				<h5>blah blah blah</h5>
			</div>
		),
		value: '5',
	},
];
stories.add(
	'default',
	() => <RadioPanel data={defaultData} name={'radio'} callback={func} />,
	{
		jest: ['RadioPanel.test.jsx'],
	}
);

stories.add(
	'icon',
	() => <RadioPanel data={iconData} name={'radio2'} initialIndex={3} />,
	{
		jest: ['RadioPanel.test.jsx'],
	}
);

stories.add(
	'flex',
	() => <RadioPanel flex={true} data={iconData} name={'radio3'} />,
	{
		jest: ['RadioPanel.test.jsx'],
	}
);

stories.add(
	'children',
	() => <RadioPanel data={childrenData} name={'radio4'} callback={func} />,
	{
		jest: ['RadioPanel.test.jsx'],
	}
);
