# RadioPanel Component

#Use
`data` requires a array of objects, each object requires
`label` that is either a string or number
`value` that is either a string or number
`icon` that is a node/component

`flex` requires boolean if true RadioPanel will have --flex class

`name` requires a string

## Example

const dataDefault = [
{
label: 'first',
icon: <FontAwesomeIcon icon={faSearch} />,
value: '1',

    },
    {
        label: 'second',
        icon: <FontAwesomeIcon icon={faSearch} />,
        value: '2',

    },
    {
        label: 'third',
        icon: <FontAwesomeIcon icon={faSearch} />,
        value: '3',

    },
    {
        label: 'fourth',
        icon: <FontAwesomeIcon icon={faSearch} />,
        value: '4',

    },
    {
        label: 'fifth',
        icon: <FontAwesomeIcon icon={faSearch} />,
        value: '5',

    }

];

<RadioPanel
flex={true}
data={dataDefault}
name={'radio3'}
initialIndex={1}
/>
