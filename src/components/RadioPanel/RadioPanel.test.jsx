import React from 'react';
import RadioPanel from './RadioPanel';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/pro-solid-svg-icons';

const dataDefault = [
	{
		label: 'first',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '1',
	},
	{
		label: 'second',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '2',
	},
	{
		label: 'third',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '3',
	},
	{
		label: 'fourth',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '4',
	},
	{
		label: 'fifth',
		icon: <FontAwesomeIcon icon={faSearch} />,
		value: '5',
	},
];

describe('RadioPanel is RadioPanel', () => {
	it('should consist of some options', () => {
		let component = mount(<RadioPanel />);
		expect(component.find('RadioPanel').exists()).toBeTruthy();
	});

	it('should be flex if prop is true', () => {
		let component = mount(<RadioPanel data={dataDefault} flex={true} />);
		expect(component.find('.RadioPanel__ul--flex').exists()).toBeTruthy();
	});

	it('should have name that matches prop', () => {
		let component = mount(<RadioPanel data={dataDefault} name={'testing'} />);
		let input = component.find('.RadioPanel input');
		expect(
			component.find('.RadioPanel input[name="testing"]').exists()
		).toBeTruthy();
	});

	it('should call the provided callback', () => {
		let fn = jest.fn();
		let component = mount(
			<RadioPanel data={dataDefault} name={'testing'} callback={fn} />
		);
		let input = component
			.find('.RadioPanel input')
			.first()
			.simulate('click', { event: () => undefined });

		expect(fn).toHaveBeenCalled();
	});
});
