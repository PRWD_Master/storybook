import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './RadioPanel.scss';

export default class RadioPanel extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedRadio: this.props.initialIndex,
		};
	}

	componentDidMount() {
		this.props.initialIndex &&
			this[`listItem${this.props.initialIndex}`].click();
	}

	onClickRadio(radio, event) {
		if (radio !== this.state.selectedRadio) {
			this.setState({ selectedRadio: radio });
			const input = event.target.closest('li');
			input.click();
			this.props.callback && this.props.callback(radio, input);
		}
	}
	render() {
		const {
			props: { name, data, flex },
			state: { selectedRadio },
		} = this;

		return (
			<div className="RadioPanel">
				<ul
					className={classnames('RadioPanel__ul', {
						'RadioPanel__ul--flex': flex,
					})}
				>
					{data &&
						data.map((item, i) => {
							let id = `${name}_${i}`;
							return (
								<li
									key={id}
									className={`RadioPanel__li ${
										i === selectedRadio ? 'RadioPanel__li--selected' : ''
									}`}
									onClick={(event) => this.onClickRadio(i, event)}
									ref={(c) => (this[`listItem${i}`] = c)}
								>
									{item.icon && <i className="RadioPanel__icon">{item.icon}</i>}
									<input
										className="RadioPanel__radio"
										type="radio"
										id={id}
										name={name}
										value={item.value}
									/>
									<label className="RadioPanel__label" htmlFor={id}>
										{item.label}
									</label>
									{item.children && (
										<div className="RadioPanel__content">{item.children}</div>
									)}
								</li>
							);
						})}
				</ul>
			</div>
		);
	}
}

RadioPanel.propTypes = {
	flex: PropTypes.bool,
	data: PropTypes.arrayOf(
		PropTypes.shape({
			icon: PropTypes.node,
			label: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
			value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
		})
	),
	name: PropTypes.string,
	callback: PropTypes.func,
	initialIndex: PropTypes.number,
};
