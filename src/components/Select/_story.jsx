import React from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, array } from "@storybook/addon-knobs";
import { withTests } from "@storybook/addon-jest";

import results from "../../../jest-test-results.json";
import Select from "./Select";

const stories = storiesOf("Select", module)
  .addDecorator(withKnobs)
  .addDecorator(withTests({ results  }));

  const label = "label";
  const options = ["option 1", "option 2", "option 3", "option 4"];
  const separator = ', ';

  const selectObj = () => {
    return {
      label: text("label", label),
      options: array("options", options, separator)
    };
  };
  
  
stories.add("default", () => <Select {...selectObj()}/> , {
   jest : ["Select.test.jsx"]
});