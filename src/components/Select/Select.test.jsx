import React from 'react';
import Select from './Select';
import { configure, shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Select', () => {
    it('should consist of some options', () => {
        //expect(true).toBeTruthy();
        let component = mount(<Select options={["option1", "option2", "option3"]} />)
        expect(component.find('option').exists()).toBeTruthy();
    })
})