import React, {Component} from "react";
import "./Select.scss";

export default class Select extends Component {
    render() {
      const {
        options, 
        label
      } = this.props;

      return (
        <div className="Select">
          <label className="Select__label">{label}</label>
          <select className="Select__select">
            {options && options.length && options.map((item, i) => <option key={i} value={i} >{item}</option>)}
          </select>
        </div>
      );
  };
}
