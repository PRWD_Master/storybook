import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';

import Accordion from './Accordion';
const stories = storiesOf('Accordion', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));
import {
	faHandPointDown,
	faHandPointLeft,
} from '@fortawesome/pro-solid-svg-icons';

let items = [
	{ title: 'This is title A', children: '<h2>Hello1</h2><p>WOW1</p>' },
	{ title: 'This is title B', children: '<h2>Hello2</h2><p>WOW2</p>' },
	{ title: 'This is title C', children: '<h2>Hello3</h2><p>WOW3</p>' },
	{ title: 'This is title D', children: '<h2>Hello4</h2><p>WOW4</p>' },
	{ title: 'This is title E', children: '<h2>Hello5</h2><p>WOW5</p>' },
];
stories.add('default', () => <Accordion items={items} />, {
	jest: ['Accordion.test.jsx'],
});

stories.add(
	'hand icon',
	() => (
		<Accordion
			items={items}
			selectedIndex={3}
			icon={faHandPointDown}
			selectedIcon={faHandPointLeft}
		/>
	),
	{
		jest: ['Accordion.test.jsx'],
	}
);
