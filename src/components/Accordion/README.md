# Accordion Component

## Use

Requires an array of `items`, each with `title` and `body`.
Can accept `initialIndex` which opens the given index.

```

## Example
let items = [{
        title: 'This is title A',
        body: '<h2>Hello1</h2><p>WOW1</p>'
    },{
        title: 'This is title B',
         body: '<h2>Hello2</h2><p>WOW2</p>'
    }
    ...
];

let initiallyOpenIndex = 1;
<Accordion
    items={items}
    initialIndex={initiallyOpenIndex}
/>



```
