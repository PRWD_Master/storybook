import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Accordion.scss';
import classnames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown, faChevronLeft } from '@fortawesome/pro-solid-svg-icons';

export default class Accordion extends Component {
	constructor(props) {
		super(props);
		this.state = {
			initialIndex: props.initialIndex >= 0 ? props.initialIndex : -1,
		};
	}

	onClick(i) {
		this.state.initialIndex == i
			? this.setState({ initialIndex: -1 })
			: this.setState({ initialIndex: i });
	}

	renderIcon(i) {
		if (this.state.initialIndex === i) {
			return this.props.selectedIcon ? this.props.selectedIcon : faChevronLeft;
		} else {
			return this.props.icon ? this.props.icon : faChevronDown;
		}
	}

	renderItem(item, i) {
		return (
			<div className="Accordion__item" key={`AccordionItem${i}`}>
				<div
					className={classnames('Accordion__title', {
						'Accordion__title--open': this.state.initialIndex === i,
					})}
					onClick={() => this.onClick(i)}
				>
					<span className="Accordion__titleInner">{item.title}</span>

					<span className="Accordion__icon">
						<FontAwesomeIcon icon={this.renderIcon(i)} />
					</span>
				</div>
				<div
					className={classnames('Accordion__body', {
						'Accordion__body--open': this.state.initialIndex === i,
					})}
				>
					<div
						className="Accordion__innerBody"
						dangerouslySetInnerHTML={{ __html: item.children }}
					/>
				</div>
			</div>
		);
	}
	render() {
		return (
			<div className="Accordion">
				{this.props.items &&
					this.props.items.map((item, i) => {
						return this.renderItem(item, i);
					})}
			</div>
		);
	}
}

Accordion.propTypes = {
	items: PropTypes.array,
	initialIndex: PropTypes.number,
	icon: PropTypes.object,
	selectedIcon: PropTypes.object,
};
