import React from 'react';
import Accordion from './Accordion';
import { configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

let items = [
	{ title: 'This is title A', children: '<h2>Hello1</h2><p>WOW1</p>' },
	{ title: 'This is title B', children: '<h2>Hello2</h2><p>WOW2</p>' },
	{ title: 'This is title C', children: '<h2>Hello3</h2><p>WOW3</p>' },
	{ title: 'This is title D', children: '<h2>Hello4</h2><p>WOW4</p>' },
	{ title: 'This is title E', children: '<h2>Hello5</h2><p>WOW5</p>' },
];
describe('Accordion', () => {
	it('should render the full set of topics', () => {
		let component = mount(<Accordion items={items} />);
		expect(component.find('.Accordion__item').length).toBe(5);
	});

	it('should render the full set of content', () => {
		let component = mount(<Accordion items={items} />);
		expect(component.find('.Accordion__body').length).toBe(5);
	});

	it('should only show when an initial Index is given', () => {
		let component = mount(<Accordion items={items} initialIndex={1} />);
		expect(component.find('.Accordion__body--open').length).toBe(1);
	});

	it('should not show any open by default', () => {
		let component = mount(<Accordion items={items} />);
		expect(component.find('.Accordion__body--open').length).toBe(0);
	});

	it('should show titles', () => {
		let component = mount(<Accordion items={items} />);
		let inner = component.find('.Accordion__titleInner');
		expect(inner.at(0).props().children).toBe('This is title A');
		expect(inner.at(1).props().children).toBe('This is title B');
		expect(inner.at(2).props().children).toBe('This is title C');
	});

	it('should show descriptions', () => {
		let component = mount(<Accordion items={items} />);
		let inner = component.find('.Accordion__body');
		expect(
			inner.at(0).props().children.props.dangerouslySetInnerHTML.__html
		).toBe('<h2>Hello1</h2><p>WOW1</p>');
		expect(
			inner.at(1).props().children.props.dangerouslySetInnerHTML.__html
		).toBe('<h2>Hello2</h2><p>WOW2</p>');
		expect(
			inner.at(2).props().children.props.dangerouslySetInnerHTML.__html
		).toBe('<h2>Hello3</h2><p>WOW3</p>');
	});
});
