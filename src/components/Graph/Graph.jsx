import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Chart } from 'react-google-charts';
import './Graph.scss';

export default class Graph extends Component {
	render() {
		const {
			width,
			height,
			chart_type,
			loader,
			data,
			options,
			root_props,
		} = this.props;

		return (
			<div className="Graph">
				<Chart
					width={width}
					height={height}
					chartType={chart_type}
					loader={loader}
					data={data}
					options={options}
					rootProps={root_props}
				/>
			</div>
		);
	}
}

Graph.propTypes = {
	width: PropTypes.string,
	height: PropTypes.string,
	chartType: PropTypes.string,
	loader: PropTypes.string,
	data: PropTypes.object,
	options: PropTypes.object,
	rootProps: PropTypes.object,
};
