import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';

import Graph from './Graph';
const stories = storiesOf('Graph', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

stories.add('default', () => <Graph>{text('label', 'default')} </Graph>, {
	jest: ['Graph.test.jsx'],
});

stories.add(
	'pie chart',
	() => (
		<Graph
			width={'500px'}
			height={'300px'}
			chart_type={'PieChart'}
			loader={<div>Loading Chart</div>}
			data={[['Client', 'Hours'], ['Used', 58], ['Remaining', 40]]}
			options={{
				title: 'Client hours',
				pieSliceText: 'label',
			}}
			root_props={{ 'data-testid': '1' }}
		/>
	),
	{
		jest: ['Graph.test.jsx'],
	}
);

stories.add(
	'bar chart',
	() => (
		<Graph
			width={'500px'}
			height={'300px'}
			chart_type={'BarChart'}
			loader={<div>Loading Chart</div>}
			data={[
				['City', '2010 Population', '2000 Population'],
				['New York City, NY', 8175000, 8008000],
				['Los Angeles, CA', 3792000, 3694000],
				['Chicago, IL', 2695000, 2896000],
				['Houston, TX', 2099000, 1953000],
				['Philadelphia, PA', 1526000, 1517000],
			]}
			options={{
				title: 'Population of Largest U.S. Cities',
				chartArea: { width: '50%' },
				isStacked: true,
				hAxis: {
					title: 'Total Population',
					minValue: 0,
				},
				vAxis: {
					title: 'City',
				},
			}}
			// For tests
			root_props={{ 'data-testid': '3' }}
		/>
	),
	{
		jest: ['Graph.test.jsx'],
	}
);

stories.add(
	'combo chart',
	() => (
		<Graph
			width={'100%'}
			height={'300px'}
			chart_type={'ComboChart'}
			loader={<div>Loading Chart</div>}
			data={[
				[
					'Month',
					'Bolivia',
					'Ecuador',
					'Madagascar',
					'Papua New Guinea',
					'Rwanda',
					'Average',
				],
				['2004/05', 165, 938, 522, 998, 450, 614.6],
				['2005/06', 135, 1120, 599, 1268, 288, 682],
				['2006/07', 157, 1167, 587, 807, 397, 623],
				['2007/08', 139, 1110, 615, 968, 215, 609.4],
				['2008/09', 136, 691, 629, 1026, 366, 569.6],
			]}
			options={{
				title: 'Monthly Coffee Production by Country',
				vAxis: { title: 'Cups' },
				hAxis: { title: 'Month' },
				seriesType: 'bars',
				series: { 5: { type: 'line' } },
			}}
			root_props={{ 'data-testid': '1' }}
		/>
	),
	{
		jest: ['Graph.test.jsx'],
	}
);

stories.add(
	'combo chart',
	() => (
		<Graph
			width={'100%'}
			height={'300px'}
			chart_type="Timeline"
			loader={<div>Loading Chart</div>}
			data={[
				[
					{ type: 'string', id: 'President' },
					{ type: 'date', id: 'Start' },
					{ type: 'date', id: 'End' },
				],
				['Washington', new Date(1789, 3, 30), new Date(1797, 2, 4)],
				['Adams', new Date(1797, 2, 4), new Date(1801, 2, 4)],
				['Jefferson', new Date(1801, 2, 4), new Date(1809, 2, 4)],
			]}
			options={{
				showRowNumber: true,
			}}
			root_props={{ 'data-testid': '1' }}
		/>
	),
	{
		jest: ['Graph.test.jsx'],
	}
);
