import React from 'react';
import Graph from './Graph';
import { configure, shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Graph is Graph', () => {
    it('should consist of some options', () => {
        let component = mount(<Graph />)
        expect(component.find('Graph').exists()).toBeTruthy();
    })
})