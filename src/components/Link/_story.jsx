import React from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import { withTests } from "@storybook/addon-jest";
import results from "../../../jest-test-results.json";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp } from '@fortawesome/pro-solid-svg-icons';

import Link from "./Link";
const stories = storiesOf("Link", module)
  .addDecorator(withKnobs)
  .addDecorator(withTests({ results }));

const defaultLinkObj = () => {
  return {
    to: text('src', 'https://prwd.co.uk'),
    newTab: boolean('Target', true),
    text: text('text', 'default'),
  };
};
stories.add('default', () => <Link {...defaultLinkObj()} />, {
  jest: ['Link.test.jsx'],
});

const childrenLinkObj = () => {
  return {
    to: text('src', 'https://prwd.co.uk'),
    children: <h1> default </h1>,
  };
};
stories.add('children', () => <Link {...childrenLinkObj()} />, {
  jest: ['Link.test.jsx'],
});

const underlineLinkObj = () => {
  return {
    to: text('src', 'https://prwd.co.uk'),
    newTab: boolean('Target', true),
    text: text('text', 'default'),
    underline: boolean('Underline', true)
  };
};
stories.add('underline', () => <Link {...underlineLinkObj()} />, {
  jest: ['Link.test.jsx'],
});

const iconLeftObj = () => {
  return {
    to: text('src', 'https://prwd.co.uk'),
    newTab: boolean('Target', true),
    left: boolean('left', true),
    leftIcon: <FontAwesomeIcon icon={faThumbsUp} />,
    text: text('text', 'default'),
  };
};

stories.add('icon left', () => <Link {...iconLeftObj()} />, {
  jest: ['Link.test.jsx'],
});

const iconRightObj = () => {
  return {
    to: text('src', 'https://prwd.co.uk'),
    newTab: boolean('Target', true),
    right: boolean('right', true),
    rightIcon: <FontAwesomeIcon icon={faThumbsUp} />,
    text: text('text', 'default'),
  };
};

stories.add('icon right', () => <Link {...iconRightObj()} />, {
  jest: ['Link.test.jsx'],
});