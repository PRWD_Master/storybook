import React from 'react';
import Link from './Link';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsUp } from '@fortawesome/pro-solid-svg-icons';
configure({ adapter: new Adapter() });

describe('Link is Link', () => {
    it('should consist of Link', () => {
        let component = mount(<Link />)
        expect(component.find('Link').exists()).toBeTruthy();
    })
    // link should have href - to
    it('it should have href', () => {
        let component = mount(<Link to="https://prwd.co.uk" />)
        expect(component.find('.Link').props().href).toBe('https://prwd.co.uk');
    })

    // link newTab = _target blank
    it('it should have target set to _blank', () => {
        let component = mount(<Link newTab={true} />)
        expect(component.find('.Link').props().target).toBe('_blank');
    })

    // icon should be svg
    it('it should have svg icon', () => {
        let component = mount(<Link right={true} rightIcon={<svg></svg>} />)
        expect(component.find('.Link__icon svg').exists()).toBeTruthy();
    })

    // using font awesome component
    it('it should have fontAwesomeIcon', () => {
        let component = mount(<Link right={true} rightIcon={<FontAwesomeIcon icon={faThumbsUp} />} />)
        expect(component.find('.Link__icon svg').exists()).toBeTruthy();
        expect(component.find('.Link__icon svg').hasClass('fa-thumbs-up')).toBeTruthy();
        expect(component.find('.Link__icon [data-icon="thumbs-up"]').exists()).toBeTruthy();
    })

    // position left
    it('should be left class if left is true', () => {
        let component = shallow(<Link left={true} />)
        expect(component.find('.Link__icon--left').exists()).toBeTruthy();

    })

    // position right
    it('should be right class if right is true', () => {
        let component = shallow(<Link right={true} />)
        expect(component.find('.Link__icon--right').exists()).toBeTruthy();
    })

    // should contain some text / child? 
    it('it should have child', () => {
        let component = mount(<Link children={<h1>test</h1>} />);
        expect(component.find('.Link__container > h1').exists()).toBeTruthy();
    })

    it('should be underline class if underline is true', () => {
        let component = shallow(<Link underline={true} />);
        expect(component.find('.Link--underline').exists()).toBeTruthy();
    })


})
