import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './Link.scss';

const Link = ({ to, newTab, text, left, leftIcon, right, rightIcon, children, underline }) => {
	return (
		<a className={classnames(
			'Link',
			{
				'Link--underline': underline,
			}
		)}
			href={to}
			target={newTab ? "_blank" : ""}
		>
			{left && <i className="Link__icon Link__icon--left">{leftIcon}</i>}
			{text && <span className="Link__text">{text}</span>}
			{right && <i className="Link__icon Link__icon--right">{rightIcon}</i>}
			{children && <div className="Link__container">{children}</div>}
		</a>
	);
};

Link.propTypes = {
	to: PropTypes.string,
	newTab: PropTypes.bool,
	left: PropTypes.bool,
	leftIcon: PropTypes.node,
	right: PropTypes.bool,
	rightIcon: PropTypes.node,
	children: PropTypes.node,
	underline: PropTypes.bool
};

export default Link;