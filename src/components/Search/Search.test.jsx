import React from 'react';
import Search from './Search';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

let autoCompleteFunc = () => {
    return [
        { text: 'this is a', id: 1 },
        { text: 'this is b', id: 2 },
        { text: 'this is c', id: 3 },
        { text: 'this is d', id: 4 },
        { text: 'this is e', id: 5 },
        { text: 'this is f', id: 6 },
        { text: 'this is d', id: 7 },
        { text: 'this is h', id: 8 },
    ]
}

let submitFunc = () => {
    return true
}

let suggestionFunc = () => {
    return true
}

describe('Search component criteria', () => {
    it('should consist of some options', () => {
        let component = mount(<Search />)
        expect(component.find('Search').exists()).toBeTruthy();
    })
    it('placeholder prop should match', () => {
        let component = shallow(<Search placeholder="searchplaceholder" />)
        expect(component.find('.Search input').props().placeholder).toBe('searchplaceholder');
    })
    it('simulate autocomplete function', () => {
        let mockFunc = jest.fn(() => autoCompleteFunc());
        let component = mount(<Search autoCompleteFunction={mockFunc} suggestionLimit={5} />)
        component.find('.Search input').simulate('change');
        expect(mockFunc).toBeCalled();
    })
    it('simulate submit function', () => {
        let mockFunc = jest.fn(() => submitFunc());
        let component = mount(<Search submitFunction={mockFunc} />)
        component.find('.Search__form').simulate('submit');
        expect(mockFunc).toBeCalled();
    })
    it('should only auto complete if auto complete function is passed', () => {
        let component = mount(<Search />)
        const input = component.find('.Search__input');
        expect(component.find('.Search__li').length).toBe(0);
        input.value = 'testing';
        expect(input.value).toBe('testing');
        input.simulate('change');
        expect(component.find('.Search__li').length).toBe(0);
    })
    it('simulate autocomplete function / check suggestion limit mapping is correct', () => {
        let mockFunc = jest.fn(() => autoCompleteFunc());
        let component = mount(<Search autoCompleteFunction={mockFunc} suggestionLimit={5} />)
        const input = component.find('.Search__input');
        expect(component.find('.Search__li').length).toBe(0);
        input.value = 'testing';
        expect(input.value).toBe('testing');
        input.simulate('change');
        expect(component.find('.Search__li').length).toBe(5);
    })
    it('simulate autocomplete suggestion click', () => {
        let mockFunc = jest.fn(() => autoCompleteFunc());
        let mockFuncTwo = jest.fn(() => suggestionFunc());
        let component = mount(<Search autoCompleteFunction={mockFunc} suggestionClickFunction={mockFuncTwo} suggestionLimit={5} />)
        const input = component.find('.Search__input');
        input.value = 'testing';
        input.simulate('change');
        component.find('.Search__li').first().simulate('click');
        expect(mockFuncTwo).toBeCalled();
    })
})