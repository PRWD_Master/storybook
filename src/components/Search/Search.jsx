import React, { Component } from "react";
import PropTypes from 'prop-types';
import "./Search.scss";

export default class Search extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value: '',
			suggestions: []
		}

		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.suggestionClick = this.suggestionClick.bind(this)
	}

	autoComplete(q) {
		let runAuto = this.props.autoCompleteFunction(q);
		this.setState({
			suggestions: runAuto
		});
	}

	handleChange(event) {
		this.setState({
			value: event.target.value
		})
		{ this.props.autoCompleteFunction && this.autoComplete(event.target.value) }
	}

	handleSubmit(event) {
		event.preventDefault();
		this.props.submitFunction();
	}

	suggestionClick(suggestion) {
		this.props.suggestionClickFunction(suggestion);
	}

	render() {
		const {
			suggestionLimit,
			placeholder,
			icon,
			right,
			text,
		} = this.props;

		return (
			<form className="Search__form" onSubmit={this.handleSubmit}>
				<div className={`Search`}>
					<input className="Search__input" type="text" placeholder={placeholder ? placeholder : "Enter your search here"} onChange={this.handleChange} />
					<button type="submit" className="Search__button">
						{icon && !right && <i className="Search__icon Search__icon--left">{icon}</i>}
						{text}
						{icon && right && <i className="Search__icon Search__icon--right">{icon}</i>}
					</button>
				</div>

				{this.props.autoCompleteFunction && <ul className="Search__ul">
					{this.state.suggestions.slice(0, suggestionLimit).map((suggestion, i) => {
						return <li key={suggestion.id} className="Search__li" onClick={() => this.suggestionClick(suggestion)}> {suggestion.text} </li>
					})}
				</ul>}
			</form>
		);
	};
}

Search.propTypes = {
	autoCompleteFunction: PropTypes.func,
	submitFunction: PropTypes.func,
	suggestionClickFunction: PropTypes.func,
	suggestionLimit: PropTypes.number,
	text: PropTypes.string,
	icon: PropTypes.node,
	right: PropTypes.bool,
	placeholder: PropTypes.string,
};