# Search Component

## Use
`submitFunction` Requires a function for onSubmit

`autoCompleteFunction` requires a function that passes back and array of objects
    - if autocomplete is not required dont pass the prop

`suggestionClickFunction` requires a function for onClick 

`suggestionLimit` requires a number, this limits how many objects gets mapped out when autoCompleteFunction is running

`text` requires string this is the search button text

`icon` requires a node, this can be either `<FontAwesomeIcon icon="" />` or `<svg>`

`right` requires boolean ? icon is rendered on right side : icon is rendered on left side

`placeholder` requires a string this changes input placeholder attribute value

## Example
// function
let autoCompleteFunc = () => {
  return [
    { text: 'this is a', id: 1 },
    { text: 'this is b', id: 2 },
    { text: 'this is c', id: 3 },
    { text: 'this is d', id: 4 },
    { text: 'this is e', id: 5 },
  ]
}

// function
let submitFunc = () => {
  alert('test');
}

// function
let suggestionFunc = (suggestion) => {
  alert(JSON.stringify(suggestion));
}

<Search
    submitFunction={submitFunc}
    autoCompleteFunction={autoCompleteFunc}
    suggestionClickFunction={suggestionFunc}
    suggestionLimit={5}
    text={'search'}
    icon={<FontAwesomeIcon icon="faThumbsUp" />}
    right={true}
    placeholder={'enter search here'}
/>