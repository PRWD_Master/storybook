import React from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import { withTests } from "@storybook/addon-jest";
import results from "../../../jest-test-results.json";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/pro-solid-svg-icons'

import Search from "./Search";
const stories = storiesOf("Search", module)
  .addDecorator(withKnobs)
  .addDecorator(withTests({ results }));

let autoCompleteFunc = () => {
  return [
    { text: 'this is a', id: 1 },
    { text: 'this is b', id: 2 },
    { text: 'this is c', id: 3 },
    { text: 'this is d', id: 4 },
    { text: 'this is e', id: 5 },
    { text: 'this is f', id: 6 },
    { text: 'this is d', id: 7 },
    { text: 'this is h', id: 8 },
  ]
}

let submitFunc = () => {
  alert('test');
}

let suggestionFunc = (suggestion) => {
  alert(JSON.stringify(suggestion));
}
stories.add(
  "default",
  () => (
    <Search text="Search" icon={<FontAwesomeIcon icon={faSearch} />} placeholder="testing placeholder text" autoCompleteFunction={autoCompleteFunc} submitFunction={submitFunc} suggestionClickFunction={suggestionFunc} suggestionLimit={5} />
  ),
  {
    jest: ["Search.test.jsx"]
  }
);

const searchObj = () => {
  return {
    submitFunction: submitFunc,
    autoCompleteFunction: autoCompleteFunc,
    suggestionClickFunction: suggestionFunc,
    suggestionLimit: number('Suggestion Limit', 5),
    text: text('Button Text', 'Search'),
    icon: <FontAwesomeIcon icon={faSearch} />,
    placeholder: text('Placeholder', 'Enter Search Term'),
  };
};

stories.add('default', () => <Search {...searchObj()} />, {
  jest: ['Search.test.jsx'],
});