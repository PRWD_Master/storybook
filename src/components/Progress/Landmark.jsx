import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Progress.scss';
import classnames from 'classnames';

export default class Landmark extends Component {
	render() {
		return (
			<div className={classnames('Landmark')}>
				<div
					className={classnames(
						'Landmark__Bar',
						{
							'Landmark__Bar--completed':
								this.props.completed || this.props.current,
						},
						{
							'Landmark__Bar--hidden': this.props.index == 0,
						}
					)}
				/>
				<div
					className={classnames(
						'Landmark__Dot',
						{
							'Landmark__Dot--completed': this.props.completed,
						},
						{
							'Landmark__Dot--active': this.props.current,
						}
					)}
				>
					{this.props.completed && this.props.icon}
				</div>
				<div
					className={classnames(
						'Landmark__Bar',
						{
							'Landmark__Bar--completed':
								this.props.completed && !this.props.current,
						},
						{
							'Landmark__Bar--hidden':
								this.props.index == this.props.length - 1,
						}
					)}
				/>
			</div>
		);
	}
}

Landmark.propTypes = {
	completed: PropTypes.bool,
	current: PropTypes.bool,
	index: PropTypes.number,
	length: PropTypes.number,
	icon: PropTypes.object,
};
