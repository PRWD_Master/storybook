import React from 'react';
import Progress from './Progress';
import { configure, shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Progress is Progress', () => {
    it('should consist of some options', () => {
        let component = mount(<Progress />)
        expect(component.find('Progress').exists()).toBeTruthy();
    })
})