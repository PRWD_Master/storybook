import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Progress.scss';
import Landmark from './Landmark';
import classnames from 'classnames';

export default class Progress extends Component {
	render() {
		let steps = this.props.steps || [];

		return (
			<div className="Progress">
				<div className="Progress__Titles">
					{steps.map((step) => (
						<div
							className={classnames(
								'Progress__Title',
								{
									'Progress__Title--active': step.active,
								},
								{
									'Progress__Title--completed': step.complete,
								}
							)}
						>
							<div className="Progress__TitleInner">{step.number}</div>
							<div className="Progress__TitleInner">{step.name}</div>
						</div>
					))}
				</div>

				<div className="Progress__Bar">
					{steps.map((step, i) => (
						<Landmark
							completed={step.complete}
							current={step.active}
							index={i}
							length={steps.length}
							icon={step.icon}
						/>
					))}
				</div>
			</div>
		);
	}
}

Progress.propTypes = {
	steps: PropTypes.array,
};
