import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/pro-solid-svg-icons';

import Progress from './Progress';
const stories = storiesOf('Progress', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

let steps = [
	{
		name: 'Property Overview',
		complete: true,
		active: false,
		number: '01',
		icon: <FontAwesomeIcon icon={faCheck} />,
	},
	{
		name: 'Estimate',
		complete: true,
		active: false,
		icon: <FontAwesomeIcon icon={faCheck} />,
	},
	{
		name: 'Your Details',
		complete: false,
		active: true,
		icon: <FontAwesomeIcon icon={faCheck} />,
	},
	{
		name: 'Overview',
		complete: false,
		active: false,
		icon: <FontAwesomeIcon icon={faCheck} />,
	},
];
stories.add('default', () => <Progress steps={steps} />, {
	jest: ['Progress.test.jsx'],
});
