import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs, text, boolean, number } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import results from '../../../jest-test-results.json';

import Button from './Button';
const stories = storiesOf('Button', module)
	.addDecorator(withKnobs)
	.addDecorator(withTests({ results }));

stories.add('default', () => <Button>{text('label', 'default')} </Button>, {
	jest: ['Button.test.jsx'],
});

stories.add(
	'disabled',
	() => (
		<Button disabled={boolean('Disabled', true)}>
			{text('label', 'default')}{' '}
		</Button>
	),
	{
		jest: ['Button.test.jsx'],
	}
);

stories.add(
	'primary',
	() => (
		<Button primary={boolean('Primary', true)}>
			{text('label', 'default')}{' '}
		</Button>
	),
	{
		jest: ['Button.test.jsx'],
	}
);

stories.add(
	'secondary',
	() => (
		<Button secondary={boolean('Secondary', true)}>
			{text('label', 'default')}{' '}
		</Button>
	),
	{
		jest: ['Button.test.jsx'],
	}
);
