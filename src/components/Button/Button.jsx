import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './Button.scss';

const Button = ({ onClick, disabled, children, primary, secondary }) => {
	return (
		<button
			className={classnames(
				'Button',
				{
					'Button--disabled': disabled,
				},
				{
					'Button--primary': primary,
				},
				{
					'Button--secondary': secondary,
				}
			)}
			onClick={onClick}
			disabled={disabled}
		>
			{children}
		</button>
	);
};

Button.propTypes = {
	onClick: PropTypes.func,
	disabled: PropTypes.bool,
	children: PropTypes.node,
	primary: PropTypes.bool,
	secondary: PropTypes.bool,
};

export default Button;
