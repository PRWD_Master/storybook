import React from 'react';
import Button from './Button';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('Button is button', () => {
	it('should consist of some options', () => {
		let component = mount(<Button />);
		expect(component.find('button').exists()).toBeTruthy();
	});

	it('if primary it should have class', () => {
		let component = mount(<Button primary={true} />);
		expect(component.find('.Button--primary').exists()).toBeTruthy();
	});

	it('if secondary it should have class', () => {
		let component = mount(<Button secondary={true} />);
		expect(component.find('.Button--secondary').exists()).toBeTruthy();
	});

	it('if disabled it should have class', () => {
		let component = mount(<Button disabled={true} />);
		expect(component.find('.Button--disabled').exists()).toBeTruthy();
	});
});
