import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './Tabs.scss';

export default class Tabs extends Component {
	constructor(props) {
		super(props);

		this.state = {
			activeTab: 0,
		};
	}

	onClickTab(tab) {
		if (tab !== this.state.activeTab) {
			this.setState({ activeTab: tab });
		}
	}

	render() {
		const {
			props: { data, flex, vertical, maxNo },
			state: { activeTab },
		} = this;
		let n =
			(flex && maxNo) ||
			(flex && 5) ||
			((vertical && maxNo) || (vertical && 7));
		if (n === undefined) {
			n = maxNo || data.length;
		}
		return (
			<div
				className={classnames(
					'Tabs',
					{
						'Tabs--flex': flex,
					},
					{
						'Tabs--vertical': vertical,
					}
				)}
			>
				<div className="Tabs__tab">
					<ul className="Tabs__ul">
						{data &&
							data.slice(0, n).map((item, i) => {
								return (
									<li
										key={`Tab__li--${i}`}
										className={`Tabs__li ${
											i === activeTab ? 'Tabs__li--active' : ''
										}`}
										onClick={() => this.onClickTab(i)}
									>
										{item.title}
									</li>
								);
							})}
					</ul>
				</div>

				<div className="Tabs__container">
					{data &&
						data.slice(0, n).map((item, i) => {
							if (i !== activeTab) return undefined;
							return (
								<div
									key={`Tab__content--${i}`}
									id={`tab-content-${item.title.replace(' ', '')}`}
									className="Tabs__content"
									dangerouslySetInnerHTML={{ __html: item.children }}
								/>
							);
						})}
				</div>
			</div>
		);
	}
}

Tabs.propTypes = {
	data: PropTypes.array.isRequired,
	flex: PropTypes.bool,
	vertical: PropTypes.bool,
	maxNo: PropTypes.number,
};
