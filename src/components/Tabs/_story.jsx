import React from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import { withTests } from "@storybook/addon-jest";
import results from "../../../jest-test-results.json";

import Tabs from "./Tabs";
const stories = storiesOf("Tabs", module)
  .addDecorator(withKnobs)
  .addDecorator(withTests({ results }));

const dataDefault = [
  {
    title: 'title 1',
    children: { __html: '<h1> title 1 </h1>' },
  },
  {
    title: 'title 2',
    children: { __html: '<h1> title 2 </h1>' },
  },
  {
    title: 'title 3',
    children: { __html: '<h1> title 3 </h1>' },
  },
  {
    title: 'title 4',
    children: { __html: '<h1> title 4 </h1>' },
  },
  {
    title: 'title 5',
    children: { __html: '<h1> title 5 </h1>' },
  },
  {
    title: 'title 6',
    children: { __html: '<h1> title 6 </h1>' },
  },
  {
    title: 'title 7',
    children: { __html: '<h1> title 7 </h1>' },
  },
  {
    title: 'title 8',
    children: { __html: '<h1> title 8 </h1>' },
  },
];

const defaultDataObj = () => {
  return {
    flex: boolean('Flex', false),
    data: dataDefault,
    maxNo: 5,
  };
};

stories.add('default', () => <Tabs {...defaultDataObj()} />, {
  jest: ['Tabs.test.jsx'],
});


const flexDataObj = () => {
  return {
    flex: boolean('Flex', true),
    data: dataDefault,
  };
};

stories.add('flex', () => <Tabs {...flexDataObj()} />, {
  jest: ['Tabs.test.jsx'],
});

const verticalDataObj = () => {
  return {
    vertical: boolean('Vertical', true),
    data: dataDefault,
  };
};

stories.add('vertical', () => <Tabs {...verticalDataObj()} />, {
  jest: ['Tabs.test.jsx'],
});