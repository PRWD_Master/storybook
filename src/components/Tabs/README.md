# Tabs Component

## Use
`data` requires a array of objects, each object requires 
    `title` that is a string
    `children` that is a node/component

`flex` requires boolean if true Tabs will have --flex class

`vertical` requires boolean if true Tabs will have --vertical class

## Example
const dataDefault = [
    {
        title: 'title 1',
        children: <h1> title 1 </h1>
    },
    {
        title: 'title 2',
        children: <h1> title 2 </h1>
    },
    {
        title: 'title 3',
        children: <h1> title 3 </h1>
    }
]

<Tabs
    data={submitFunc}
    flex={true}
    vertical={false}
/>