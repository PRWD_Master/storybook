import React from 'react';
import Tabs from './Tabs';
import { configure, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
const dataDefault = [
	{
		title: 'title 1',
		children: { __html: '<h1> title 1 </h1>' },
	},
	{
		title: 'title 2',
		children: { __html: '<h1> title 2 </h1>' },
	},
	{
		title: 'title 3',
		children: { __html: '<h1> title 3 </h1>' },
	},
	{
		title: 'title 4',
		children: { __html: '<h1> title 4 </h1>' },
	},
	{
		title: 'title 5',
		children: { __html: '<h1> title 5 </h1>' },
	},
	{
		title: 'title 6',
		children: { __html: '<h1> title 6 </h1>' },
	},
	{
		title: 'title 7',
		children: { __html: '<h1> title 7 </h1>' },
	},
	{
		title: 'title 8',
		children: { __html: '<h1> title 8 </h1>' },
	},
];

describe('Tabs is Tabs', () => {
	it('should consist of some options', () => {
		let component = shallow(<Tabs data={dataDefault} />);
		expect(component.find('.Tabs').exists()).toBeTruthy();
	});

	it('should be flex if prop is true', () => {
		let component = mount(<Tabs data={dataDefault} flex={true} />);
		expect(component.find('.Tabs--flex').exists()).toBeTruthy();
	});

	it('if flex only 5 items should be mapped', () => {
		let component = mount(<Tabs data={dataDefault} flex={true} />);
		expect(component.find('.Tabs--flex').exists()).toBeTruthy();
		expect(component.find('.Tabs__li').length).toBe(5);
	});

	it('should be vertical if prop is true', () => {
		let component = mount(<Tabs data={dataDefault} vertical={true} />);
		expect(component.find('.Tabs--vertical').exists()).toBeTruthy();
	});

	it('if vertical only 7 items should be mapped', () => {
		let component = mount(<Tabs data={dataDefault} vertical={true} />);
		expect(component.find('.Tabs--vertical').exists()).toBeTruthy();
		expect(component.find('.Tabs__li').length).toBe(7);
	});

	it('if max no passed', () => {
		let component = mount(<Tabs data={dataDefault} maxNo={3} vertical={true} />);
		expect(component.find('.Tabs--vertical').exists()).toBeTruthy();
		expect(component.find('.Tabs__li').length).toBe(3);
	});
	// function should func / change state
	it('on click function should run and change state', () => {
		let component = mount(<Tabs data={dataDefault} />)
		component.find('.Tabs__li').at(2).simulate('click');
		expect(component.find('#tab-content-title3').exists()).toBeTruthy();
	})
})
