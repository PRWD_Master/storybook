import React from 'react';
import placeholder from './placeholder';
import { configure, shallow, mount} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

describe('placeholder is placeholder', () => {
    it('should consist of some options', () => {
        let component = mount(<placeholder />)
        expect(component.find('placeholder').exists()).toBeTruthy();
    })
})