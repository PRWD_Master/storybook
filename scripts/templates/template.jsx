import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './placeholder.scss';

export default class placeholder extends Component {
	render() {
		return <div className="placeholder">placeholder</div>;
	}
}

placeholder.propTypes = {};
