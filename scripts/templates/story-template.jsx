import React from "react";
import { storiesOf } from "@storybook/react";
import { withKnobs, text, boolean, number } from "@storybook/addon-knobs";
import { withTests } from "@storybook/addon-jest";
import results from "../../../jest-test-results.json";

import placeholder from "./placeholder";
const stories = storiesOf("placeholder", module)
  .addDecorator(withKnobs)
  .addDecorator(withTests({ results  }));

stories.add(
  "default",
  () => (
    <placeholder>
      {text("label", "default")}{" "}
    </placeholder>
  ),
  {
    jest: ["placeholder.test.jsx"]
  }
);

