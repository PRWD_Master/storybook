#!/usr/bin/env node
// get component name
const fs = require('fs');
const [, , ...args] = process.argv;
const dir = `./${args}`;
let componentName = args[0];
componentName = componentName.charAt(0).toUpperCase() + componentName.slice(1);
console.log(`* create:component ${componentName}`);

var readWriteAsync = (filePath, fileName, compName) => {
	fs.readFile(filePath, 'utf-8', function(err, data) {
		if (err) throw err;
		var newValue = data.replace(/placeholder/gim, compName);

		fs.writeFile(
			`src/components/${compName}/${fileName}`,
			newValue,
			'utf-8',
			function(err) {
				if (err) throw err;
				console.log(`   > created ${componentName} ${fileName}`);
			}
		);
	});
};

// create component folder
if (!fs.existsSync(dir)) {
	fs.mkdirSync(`src/components/${componentName}`);
	createFiles();
} else {
	return 'file exsists';
}

function createFiles() {
	// within folder create index.js
	fs.appendFile(
		`src/components/${componentName}/index.js`,
		`import ${componentName} from './${componentName}';\n\nexport default ${componentName};`,
		function(err) {
			if (err) throw err;
			console.log(`   > created ${componentName} index.js`);
		}
	);
	// within folder create _story.js
	readWriteAsync(
		'scripts/templates/story-template.jsx',
		'_story.jsx',
		componentName
	);
	// within folder create args.jsx
	readWriteAsync(
		'scripts/templates/template.jsx',
		`${componentName}.jsx`,
		componentName
	);
	// within folder create args.test.jsx
	readWriteAsync(
		'scripts/templates/template.test.jsx',
		`${componentName}.test.jsx`,
		componentName
	);
	// within folder create args.scss
	readWriteAsync(
		'scripts/templates/template.scss',
		`${componentName}.scss`,
		componentName
	);
	// within folder create args README.md
	readWriteAsync('scripts/templates/README.md', `README.md`, componentName);
}
