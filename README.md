# PRWD Component Library

- to start storybook

  > npm run storybook

- to run test

  > npm run test

  > npm run test:watch

- to create new component

  > npm run create:component `args`
