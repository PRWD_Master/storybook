import { configure, addDecorator } from '@storybook/react';
import { withOptions } from '@storybook/addon-options';
import reset from './reset.scss';

// automatically import all files ending in *.stories.js
const req = require.context('../src', true, /_story.js/);
function loadStories() {
	req.keys().forEach((filename) => req(filename));
}

// Option defaults:
addDecorator(
	withOptions({
		name: 'PRWD - Storybook',
		/**
		 * URL for name in top left corner to link to
		 * @type {String}
		 */
		url: '#',
		/**
		 * display panel that shows a list of stories
		 * @type {Boolean}
		 */
		showStoriesPanel: true,
		/**
		 * display panel that shows addon configurations
		 * @type {Boolean}
		 */
		showAddonPanel: true,
		/**
		 * show addon panel as a vertical panel on the right
		 * @type {Boolean}
		 */
		addonPanelInRight: true,
	})
);

configure(loadStories, module);
