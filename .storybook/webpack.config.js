const path = require('path');

module.exports = {
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					{ loader: 'style-loader' },
					{ loader: 'css-loader' },
					{ loader: 'sass-loader' },
					{
						loader: 'sass-resources-loader',
						options: {
							resources: ['./src/utils/variables/*.scss', './src/utils/mixins/*.scss'],
						},
					},
				],
				include: [path.resolve(__dirname, '../src'), path.resolve(__dirname)],
			},
		],
	},
};
