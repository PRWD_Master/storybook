const path = require('path');

module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
        resolve: { extensions: ['.js', '.jsx'] },
      },
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' },
          {
            loader: 'sass-resources-loader',
            options: {
              resources: ['./src/utils/variables/*.scss', './src/utils/mixins/*.scss'],
            },
          },
        ],
      },
      {
        test: /\.(png|gif|jpg|svg)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 50000,
          },
        },
      },
    ],
  },
  output: {
    path: path.resolve(__dirname, 'dist/'),
    publicPath: '',
    filename: 'prwd.js',
    library: '@prwd/component-library',
    libraryTarget: 'umd',
  },
  resolve: {
    extensions: [
      '.scss',
      '.js',
      '.jsx',
      '.json',
      '.png',
      '.gif',
      '.jpg',
      '.svg',
    ],
  },
};
